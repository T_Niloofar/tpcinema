<?php

use Illuminate\Database\Seeder;

class FilmesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Filmes::class,3)->create()->each(function ($film) {
            $actors = factory(App\Actors::class, 5)->make();
            $film->actors()->saveMany($actors);
           
        });
    }
}
