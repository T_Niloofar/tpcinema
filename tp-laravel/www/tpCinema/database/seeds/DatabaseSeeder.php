<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(FilmesTableSeeder::class);
         $this->call(CinemasTableSeeder::class);
         $this->call(ActorsTableSeeder::class);
         $this->call(SalonsTableSeeder::class);
         $this->call(TicketsTableSeeder::class);
         $this->call(FilmeImgTableSeeder::class);

    }
}
