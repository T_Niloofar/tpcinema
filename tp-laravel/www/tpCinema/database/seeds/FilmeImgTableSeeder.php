<?php

use Illuminate\Database\Seeder;

class FilmeImgTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\FilmeImg::class,3)->create();
    }
}
