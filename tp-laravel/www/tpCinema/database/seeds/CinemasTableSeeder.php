<?php

use Illuminate\Database\Seeder;

class CinemasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Cinemas::class,3)->create()->each(function ($cinema) {
            $salons = factory(App\Salons::class, 5)->make();
            $cinema->salons()->saveMany($salons);
        });
    }

}
