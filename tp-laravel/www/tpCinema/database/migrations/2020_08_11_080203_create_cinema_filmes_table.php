<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCinemaFilmesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cinemas_filmes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cinemas_id')->unsigned()->index();
            $table->bigInteger('filmes_id')->unsigned()->index();
            $table->foreign('cinemas_id')->references('id')->on('cinemas')
                 ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('filmes_id')->references('id')->on('filmes')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cinemas_filmes', function(blueprint $table){
            
            $table->dropForeign(['cinemas_id']);
            $table->dropColumn(['cinemas_id']);
            $table->dropForeign(['filmes_id']);
            $table->dropColumn(['filmes_id']);
        });
    }
}


	
