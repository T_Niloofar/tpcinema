<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmeImgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filme_imgs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('img');
            $table->biginteger('filme_id')->unsigned()->nullable()->index();
            $table->foreign('filme_id')->references('id')->on('filmes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filme_imgs', function(blueprint $table){
            $table->dropForeign(['filme_id']);
            $table->dropColumn(['filme_id']);
        });
    }
}
