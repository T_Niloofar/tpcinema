<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('prix');
            $table->integer('nombre');
            $table->integer('quantity');
            $table->biginteger('cinema_id')->unsigned()->nullable()->index();
            $table->foreign('cinema_id')->references('id')->on('cinemas')->onDelete('cascade');
            $table->biginteger('filme_id')->unsigned()->nullable()->index();
            $table->foreign('filme_id')->references('id')->on('filmes')->onDelete('cascade');
            $table->date('date');
            $table->time('seance');
            $table->biginteger('salon_id')->unsigned()->nullable()->index();
            $table->foreign('salon_id')->references('id')->on('salons')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets', function(blueprint $table){
            
            $table->dropForeign(['cinema_id']);
            $table->dropColumn(['cinema_id']);
            $table->dropForeign(['filme_id']);
            $table->dropColumn(['filme_id']);
            $table->dropForeign(['salon_id']);
            $table->dropColumn(['salon_id']);
        });
    }
}
