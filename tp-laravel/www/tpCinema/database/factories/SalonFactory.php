<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Salons;
use Faker\Generator as Faker;

$factory->define(Salons::class, function (Faker $faker) {
    return [
     'name' => $faker->name,
     'capasite' => $faker->numberBetween(30,150),
     'cinema_id' => $faker->numberBetween(1,3),
    ];
});
