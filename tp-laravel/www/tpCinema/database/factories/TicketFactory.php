<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Tickets;
use Faker\Generator as Faker;

$factory->define(Tickets::class, function (Faker $faker) {
    return [
        'prix' => $faker->numberBetween(15,400),
        'nombre' => $faker->numberBetween(1,150),
        'quantity' => $faker->numberBetween(1,50),
        'cinema_id' => $faker->numberBetween(1,3),
        'filme_id' => $faker->numberBetween(1,3),
        'date' => $faker->dateTimeThisMonth(),
        'seance' => $faker->time(),
    ];
});
