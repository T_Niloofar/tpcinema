<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Filmes;
use Faker\Generator as Faker;

$factory->define(Filmes::class, function (Faker $faker) {
    return [
       'name' => $faker->name,
       'duration' => $faker->numberBetween(60,120),
       'directeur' => $faker->name,
       'year' => $faker->numberBetween(1500,2020),
       'description' => $faker->sentences(3,true),
       'img' => $faker->imageUrl(800, 300),
       'tiser' => $faker->imageUrl(400, 300),
       'genre'=> $faker->name,
      // 'salon_id' => $faker->numberBetween(1,3),
    ];
});
