<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\FilmeImg;
use Faker\Generator as Faker;

$factory->define(FilmeImg::class, function (Faker $faker) {
    return [
        'img'=> $faker->imageUrl(800, 300),
        'filme_id' => $faker->numberBetween(1,3),
    ];
});
