<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Cinemas;
use Faker\Generator as Faker;

$factory->define(Cinemas::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'adresse' => $faker->text(70),
        'img'=> $faker->imageUrl(400, 300),
       
    ];
});
