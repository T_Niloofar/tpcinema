<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Actors;
use Faker\Generator as Faker;

$factory->define(Actors::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'family' => $faker->name ,
        'filme_id' => $faker->numberBetween(1,3),
        'image' => $faker->name,

    ];
});
