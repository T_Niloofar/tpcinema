<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cinemas extends Model
{
    protected $fillable = [
        'name',
        'adresse',
        'city',
        'img'
    ];


    public function salons()
    {
        return $this->hasMany(Salons::class, 'cinema_id', 'id');
    }

    public function filmes(){

        return $this->belongsToMany('App\Filmes');
    }
}
