<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'ticket_id',
        'user_id',
        'seatNumbers',
        'quantity'
    ];
}
