<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tickets extends Model
{
    protected $fillable = [
        'cinema_id',
        'date',
        'filme_id',
        'nomre',
        'prix',
        'quantity',
        'seance',
        'salon_id'
    ];
}
