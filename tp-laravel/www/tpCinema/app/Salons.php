<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salons extends Model
{ 
    protected $fillable = [
    'name',
    'cinema_id',
    'capasite'
];
    
    public function cinema()
    {
        return $this->belongsTo(Cinemas::class, 'cinema_id', 'id');
    }
    public function filmes(){
        return $this->hasMany('App\Filmes');
    }
}
