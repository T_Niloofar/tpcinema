<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filmes;
use App\Cinemas;
use App\Actors;
use App\Salons;
use Illuminate\Support\Facades\DB;

class FilmeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filmes = Filmes::all();

        return view('filmes.index')->with('filmes', $filmes);
              
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        return view('admin.filmCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  =>'required',
            'description'  =>'required',
            'img'  =>'required',
            'year'  =>'required',
            'tiser'  =>'required',
            'salon_id'  =>'required',
            'genre'  =>'required',
            'duration'  =>'required',
            'directeur'  =>'required',

        ]);
         $film = new Filmes($request->all());
         $film->save();
         return redirect('/filmes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $filme = Filmes::find($id);

        $actors = DB::table('actors')
                    ->where('filme_id', '=', $id)
                    ->get();

         $filmeImgs = DB::table('filme_imgs')
                    ->where('filme_id', '=', $id)
                    ->get();

         $cinema_filme = DB::table('cinemas_filmes')
         
                    ->where('filmes_id', '=', $id)
                    ->get();

        $cinemas = Filmes::find($id)->cinemas;
        
        $seances = DB::table('tickets')
            ->where('filme_id','=', $id)
            ->get();

        
        return view('filmes.detail')->with([
            'filme' => $filme,
            'actors' => $actors,
            'filmeImgs' => $filmeImgs,
            'cinemas' => $cinemas,
            'seances' => $seances,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function genre()
    {
        $genres = DB::table('filmes')->select('genre')->distinct()->get();
        $imgs = DB::table('filmes')->select('img','genre')->get();

       
        return view('filmes.genre')->with([
            'genres' => $genres,
            'imgs'=>$imgs
            
        ]);
    }
}
