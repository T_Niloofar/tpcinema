<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Filmes;
use App\Cinemas;


class CinemaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $filmes = Filmes::all();
        $cinemas = Cinemas::all();
         return view('filmes.cinema')->with([
             'cinemas' => $cinemas, 
             'filmes' => $filmes, 
            
         ]);
     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cinema = 1;
         return view('admin.cinemaCreate')->with([
            'cinemas' => $cinema, 
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  =>'required',
            'adresse'  =>'required',
            'img'  =>'required',
            'city'  =>'required'
        ]);
         $cinema = new Cinemas($request->all());
         $cinema->save();
         return redirect('/cinema');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(request()->ajax){
             /* $cinemas = DB::table('cinemas')
                    ->where('filme_id', '=', $id)
                    ->get();
        return view('welcome')->with('cinemas', $cinemas);
*/

        }

      

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
