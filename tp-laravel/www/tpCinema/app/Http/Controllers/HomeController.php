<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\FilmeImg;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $filmeImgs = FilmeImg::all();
        return view('home')->with('filmeImgs',$filmeImgs);
    }


    /**
     * Function of Achat ovid pour activitre les selectes
     */
    function indexSelectBox()
    {
     $filmes_list = DB::table('filmes')
         ->groupBy('filmes')
         ->get();
     return view('dynamic_dependent')->with('filmes_list', $filmes_list);
    }

    function fetch(Request $request)
    {
     $select = $request->get('select');
     $value = $request->get('value');
     $dependent = $request->get('dependent');
     $data = DB::table('filmes')
       ->where($select, $value)
       ->groupBy($dependent)
       ->get();
     $output = '<option value="">Select '.ucfirst($dependent).'</option>';
     foreach($data as $row)
     {
      $output .= '<option value="'.$row->$dependent.'">'.$row->$dependent.'</option>';
     }
     echo $output;
    }
}






 