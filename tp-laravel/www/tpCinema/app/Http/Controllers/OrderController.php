<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filmes;
use App\Cinemas;
use App\Salons;
use App\Tickets;
use App\Order;



class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'seatNumbers'   =>'required',
            'ticket_id'      =>'required',
            'user_id'        =>'required',
            'quantity'      =>'required'
        ]);
         $ordre = new Order($request->all());
         $ordre->save();
         return redirect('/filmes');
        

    }

    public function confirm(Request $request){
        $ticket = Tickets::find($request['ticket_id']);
        $filme = Filmes::find($ticket->filme_id);
        $ordre = $request->all();
        return view('filmes.cart')->with([
            'filme' => $filme,
            'ordre' => $ordre,
            'ticket' => $ticket,
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = Tickets::find($id);
        $order = Order::where('ticket_id', $id)->select('seatNumbers')->get();
        $filme = Filmes::find($ticket->filme_id);


       // $cinema = Cinemas::find($id->cinemas);

        return view('filmes.payement')->with([
            'filme' => $filme,
            'order' => $order,
            'ticket' => $ticket,
            ]);
    }

    public function achatImmediat(Request $request) {
        $ticket = Tickets::find($request['seance']);
        $order = Order::where('ticket_id', $request['seance'])->select('seatNumbers')->get();
        $filme = Filmes::find($ticket->filme_id);


       // $cinema = Cinemas::find($id->cinemas);

        return view('filmes.payement')->with([
            'filme' => $filme,
            'order' => $order,
            'ticket' => $ticket,
            ]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
