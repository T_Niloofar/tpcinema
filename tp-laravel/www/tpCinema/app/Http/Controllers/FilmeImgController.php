<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filmes;
use App\Cinemas;
use Illuminate\Support\Facades\DB;

class FilmeImgController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filmes = Filmes::simplePaginate(8);

        return view('welcome')->with('filmes', $filmes);

    }
    /** Feth pour nav bar Achat*/
     

    function fetch(Request $request)
    {
       
        if($request->get('select')=='filmes_id'){
            $select = $request->get('select');
            $value = $request->get('value');
            $dependent = $request->get('dependent');
            
            $data = DB::table('cinemas_filmes')
            ->where($select, $value)
            ->get();
            $output = '<option value="">Select '.ucfirst($dependent).'</option>';
            foreach($data as $row)
            {
                $cinema = DB::table('cinemas')->find($row->cinemas_id);
                $output .= '<option value="'.$cinema->id.'">'.$cinema->name.'</option>';
            }
            echo $output;
        }else if($request->get('select')=='cinemas'){
            $select = $request->get('select');
            $value = $request->get('value');
            $dependent = $request->get('dependent');
            
            $data = DB::table('tickets')
            ->where('cinema_id', $value)
            ->groupBy('date')
            ->get();
            $output = '<option value="">Select '.ucfirst($dependent).'</option>';
            foreach($data as $row)
            {
                $output .= '<option value="'.$row->date.'">'.$row->date.'</option>';
            }
            echo $output;
        } else if($request->get('select')=='date'){
            $select = $request->get('select');
            $value = $request->get('value');
            $dependent = $request->get('dependent');
            
            $seance = DB::table('tickets')
            ->where('date', $value)
            ->get();
            $output = '<option value="">Select '.ucfirst($dependent).'</option>';
            foreach($seance as $row)
            {
                $output .= '<option value="'.$row->id.'">'.$row->seance.'</option>';
            }
            echo $output;
        }
     
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
