<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filmes extends Model
{
    protected $fillable = [
        'name',
        'description',
        'directeur',
        'img',
        'duration',
        'genre',
        'salon_id',
        'tiser',
        'year',


    ];
    public function actors()
    {
        return $this->hasMany(Actors::class, 'filme_id', 'id');
    }
    public function salon()
    {
        return $this->blongsTo('App\Salons');
        
    }

    public function cinemas(){

        return $this->belongsToMany('App\Cinemas');
    }







}
