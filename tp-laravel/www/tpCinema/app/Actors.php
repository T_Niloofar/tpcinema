<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actors extends Model
{
    public function filme()
    {
        return $this->belongsTo(Filmes::class, 'filme_id', 'id');
    }
}
