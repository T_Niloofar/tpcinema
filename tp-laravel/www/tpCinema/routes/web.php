<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','FilmeImgController@index')->name('filmSelect');
Route::post('/fetch', 'FilmeImgController@fetch')->name('filmSelect.fetch');

//Route::get('/','CinemaController@show');


//Route::get('/', function () {
   // return view('welcome');
//});


//Route::get('/home', 'DynamicDependent@indexSelectBox');

//Route::post('home/fetch', 'DynamicDependent@fetch')->name('home.fetch');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::get('filmes/index', 'FilmeController@index')->name('filmes.index');
Route::get('genre','FilmeController@genre');

Route::resource('filmes','FilmeController');
Route::resource('salons','SalonController');

Route::resource('actors','ActorController');
Route::resource('cinema','CinemaController');
Route::get('/adminPanel','AdminController@index');

//Route::get('/welcome','FilmeImgController@index');
//Route::get('/welcome','CinemaController@show');


Route::get('payement/{id}', 'OrderController@show');
Route::post('cart', 'OrderController@confirm');
Route::post('cart/confirm', 'OrderController@store');

Route::post('/payement', 'OrderController@achatImmediat')->name('filmespayment');





