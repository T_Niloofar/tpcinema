<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Suisse Cinema</title>
      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
      {{--  
      <link rel="stylesheet" href="{{ asset('css/darkMovie.css') }}">
      --}}
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://www.klevermedia.co.uk/html_templates/movie_star_html/css/slick.css" rel="stylesheet">
      <link href="https://www.klevermedia.co.uk/html_templates/movie_star_html/css/venobox.css" rel="stylesheet">
      <link rel="stylesheet" href="{{ asset('css/welcome.css') }}">

      <!-- script -->
      <script src="{{ asset('js/modal.js') }}" defer></script>
      <script src="{{ asset('js/script.js') }}" defer></script>
      <script src="{{ asset('js/carousel.js') }}" defer></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
   </head>
   <body style="opacity: 1">
    <nav class="navbar navbar-expand-md navbar-dark shadow-sm fixed-top navbarMenu">
      <div class="container">
          <a class="navbar-brand" href="{{ url('/') }}">
              Cinema Ticket
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
              <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <!-- Left Side Of Navbar -->
              <ul class="navbar-nav mr-auto">
                  <li class="nav-item ">
                      <a class="nav-link" href="#">FILMS </a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="#">CINÉMAS</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="#">GENRE</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="#">ACCOUNT</a>
                  </li>
              </ul>

              <!-- Right Side Of Navbar -->
              <ul class="navbar-nav ml-auto">
                  <!-- Authentication Links -->
                  @guest
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                      </li>
                      @if (Route::has('register'))
                          <li class="nav-item">
                              <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                          </li>
                      @endif
                  @else
                      <li class="nav-item dropdown">
                          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                              {{ Auth::user()->name }} <span class="caret"></span>
                          </a>

                          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="{{ route('logout') }}"
                                 onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                  {{ __('Logout') }}
                              </a>

                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  @csrf
                              </form>
                          </div>
                      </li>
                  @endguest
              </ul>
          </div>
      </div>
  </nav>
     
      {{--  ..................................MODAL...........................    --}}  
      <section id="modal" style="display: none; background-color:rgb(204, 204, 243)">
         <div class="container" >
            <div class="row" style="display: flex">
               <div>
                  <img src="{{URL::to('img/cin1.jpeg')}}">
               </div>
               <div>
                  <form method="POST" action="{{ route('register') }}">
                     @csrf
                     <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                        <div class="col-md-6">
                           <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                           @error('name')
                           <span class="invalid-feedback" role="alert">
                           <strong>{{ $message }}</strong>
                           </span>
                           @enderror
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                        <div class="col-md-6">
                           <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                           @error('email')
                           <span class="invalid-feedback" role="alert">
                           <strong>{{ $message }}</strong>
                           </span>
                           @enderror
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                        <div class="col-md-6">
                           <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                           @error('password')
                           <span class="invalid-feedback" role="alert">
                           <strong>{{ $message }}</strong>
                           </span>
                           @enderror
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                        <div class="col-md-6">
                           <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
                     </div>
                     <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                           <button type="submit" class="btn btn-primary">
                           {{ __('Register') }}
                           </button>
                           <button type="button" class="btn btn-danger" onClick="modalClose()">Close</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </section>
      {{--      ..........................CAROSEL ......................... --}}
      <div id="carouselExampleCaptions" class="carousel slide mt-0" data-ride="carousel" style="border-top-right-radius: 1rem">
         <ol class="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="4"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="5"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="6"></li>
         </ol>
         <div class="carousel-inner">
            <div class="carousel-item active">
               <img src="{{URL::to('img/cin8.jpeg')}}" class="d-block w-100" alt="...">
               <div class="carousel-caption d-none d-md-block">
                  <h5>First slide label</h5>
                  <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
               </div>
            </div>
            <div class="carousel-item">
               <img src="{{URL::to('img/cin5.jpg')}}" class="d-block w-100" alt="...">
               <div class="carousel-caption d-none d-md-block">
                  <h5>First slide label</h5>
                  <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
               </div>
            </div>
            <div class="carousel-item">
               <img src="{{URL::to('img/2661193.jpg')}}" class="d-block w-100" alt="...">
               <div class="carousel-caption d-none d-md-block">
                  <h5>Second slide label</h5>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
               </div>
            </div>
            <div class="carousel-item">
               <img src="{{URL::to('img/cin7.jpg')}}" class="d-block w-100" alt="...">
               <div class="carousel-caption d-none d-md-block">
                  <h5>First slide label</h5>
                  <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
               </div>
            </div>
            <div class="carousel-item">
               <img src="{{URL::to('img/kore.jpg')}}" class="d-block w-100" alt="...">
               <div class="carousel-caption d-none d-md-block">
                  <h5>First slide label</h5>
                  <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
               </div>
            </div>
            <div class="carousel-item">
               <img src="{{URL::to('img/Spidey-3.jpg')}}" class="d-block w-100" alt="...">
               <div class="carousel-caption d-none d-md-block">
                  <h5>First slide label</h5>
                  <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
               </div>
            </div>
            <div class="carousel-item">
               <img src="{{URL::to('img/miles-morales-1.jpg')}}" class="d-block w-100" alt="...">
               <div class="carousel-caption d-none d-md-block">
                  <h5>Third slide label</h5>
                  <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
               </div>
            </div>
         </div>
         <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
         <span class="carousel-control-prev-icon" aria-hidden="true"></span>
         <span class="sr-only">Previous</span>
         </a>
         <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
         <span class="carousel-control-next-icon" aria-hidden="true"></span>
         <span class="sr-only">Next</span>
         </a>
      </div>
      {{-- ::::::::::::::::::::SCRIPT ON MENU ACHAT:::::::::::::::::--}}
      <script>
         $(document).ready(function(){
         $('.dynamic').change(function(){
         if($(this).val() != '')
          {
              var select = $(this).attr("id");
              var value = $(this).val();
              var dependent = $(this).data('dependent');
              var _token = $('input[name="_token"]').val();
              $.ajax({
                url:"{{ route('filmSelect.fetch') }}",
                method:"POST",
                data:{select:select, value:value, _token:_token, dependent:dependent},
                success:function(result)
                {
                $('#'+dependent).html(result);
                }
              })
            }
          });
         $('#filmes').change(function(){
          $('#cinemas').val('');
          $('#date').val('');
         });
         $('#cinemas').change(function(){
          $('#date').val('');
         });
         });
      </script>
      {{-- ................... ACHAT IMMEDIAT........................      --}}
      <div class="contailer" style="width: 100%; margin:auto;border-radius:2em">
         <div class="row" style="margin:0 ;text-align:center;">
            <div class="m-quick-buy">
               <form action="{{ route('filmespayment') }}" method="POST">
                  @csrf
                  <ul class=" m-auto">
                     <li class="m-quick-buy__item" >
                        <select name="filmes_id" class="form-control dynamic" id="filmes_id"  data-dependent="cinemas">
                           <option >FILME</option>
                           @foreach ($filmes as $filme)
                           <option value="{{$filme->id}}">{{$filme->name}}</option>
                           @endforeach
                        </select>
                        <input type="hidden" name="filmeName" value="">
                     </li>
                     <li class="m-quick-buy__item">
                        <select name="cinemas" class="form-control dynamic" id="cinemas"  data-dependent="date">
                           <option style="background: #5cb85c; color: #fff;" >CINEMA</option>
                        </select>
                     </li>
                     <li class="m-quick-buy__item">
                        <select name="date" class="form-control dynamic" id="date"  data-dependent="seance">
                           <option >DATE</option>
                        </select>
                     </li>
                     <li class="m-quick-buy__item" >
                        <select name="seance" class="form-control dynamic " id="seance">
                           <option >HORAIRE & VER...</option>
                        </select>
                     </li>
                     <li class="m-quick-buy__item" >
                        <button class="form-control btnAchat">ACHAT IMMEDIAT</button>
                     </li>
                  </ul>
               </form>
            </div>
         </div>
      </div>
      {{-- ...........................................Images of films................................... --}}
      <section style="background-color:#e9e1d6;" >
         <div class="container-fluid" >
            <div class="row justify-content-center ">
               <h1 class="mt-3">tedtedtzed</h1>
            </div>
            <div class="row justify-content-center" >
               @foreach ($filmes as $filme)
               <a href="{{URL::to('filmes/' . $filme->id)}}" class="col-sm-6 col-lg-3">
                  <div class="m-2" >
                     <div class="welcomeImgs w-100">
                        <img src="{{$filme->img}}" class="w-100" alt="5 Terre" style="width:100%">
                        <div class="containerimg">
                           <p>{{$filme->name}}</p>
                        </div>
                     </div>
                  </div>
               </a>
               @endforeach
            </div>
         </div>
      </section>
      {{-- ::::::::::::::::::::::CAROUSELL ARTISTS:::::::::::::::::::--}}
      <section class="ArtisiSection" >
         <div class="row justify-content-center">
            <h1 class="artisttitr">LES ACTORES ET LES ACTRISSE</h1>
         </div>
         <p class="artisttext">QUI EST VOTRE ACTER FAVORITEEZ?</p>
         <span></span>
         <article>
            <div class="artistDiv">
               <ul class="artisiUl">
                  <li class="artistLi"><img class="artistImg" src="{{URL::to('img/ac1.jpg')}}" /></li>
                  <li class="artistLi"><img class="artistImg" src="{{URL::to('img/ac2.jpg')}}" /></li>
                  <li class="artistLi"><img class="artistImg" src="{{URL::to('img/ac5.jpg')}}" /></li>
                  <li class="artistLi"><img class="artistImg" src="{{URL::to('img/ac3.jpg')}}" /></li>
                  <li class="artistLi"><img class="artistImg" src="{{URL::to('img/ac7.jpg')}}" /></li>
                  <li class="artistLi"><img class="artistImg" src="{{URL::to('img/ac4.jpg')}}" /></li>
                  <li class="artistLi"><img class="artistImg" src="{{URL::to('img/ac6.jpg')}}" /></li>
                  <li class="artistLi"><img class="artistImg" src="{{URL::to('img/ac8.jpg')}}" /></li>
               </ul>
            </div>
         </article>
      </section>
      {{-- ::::::::::::::::::::::CAROUSELL ARTISTS END:::::::::::::::::::--}}
      {{-- :::::::::::::::::::::::::SHOW FILM START::::::::::::::::::::::--}}
      <video controls src=""></video>
      {{-- :::::::::::::::::::::::::SHOW FILM END::::::::::::::::::::::::--}}
      {{-- ::::::::::::::::::::::::FOTTER START HERE:::::::::::::::::::--}}
      <section class="footer" style="background-color: coral">
{{--          <div class="mainfooter">
            <div class="row footer">
               <div >
                  <h2>ABOUT US</h2>
                  <P >un imprimeur anonyme assembla
                     ensemble des morceaux de texte pour
                     éaliser un livre spécimen de polices
                     de texte. Il n'a pas fait que survivre 
                     cinq siècles, mais s'est aussi adapté àiq
                  </P>
               </div>
               <div>
                  <h2>Contact us</h2>
                  <ul>
                     <li><i><img class="footerimg" src="{{URL::to('img/icon10.png')}}"></i>Facebook</li>
                     <li><i><img class="footerimg" src="{{URL::to('img/icon11.jpeg')}}" alt=""></i>Instagram</li>
                     <li><i><img class="footerimg" src="{{URL::to('img/icon12.png')}}" alt=""></i>Twitter</li>
                  </ul>
               </div>
            </div>
         </div> --}}
        <div class="container-fluid">
          <div class="row">
            <div class="col-8">
            <ul class="list-group list-group-horizontal-xl">
              <li class="list-group-item">FILMS</li>
              <li class="list-group-item">CINÉMAS</li>
              <li class="list-group-item">GENRE</li>
              @if (Route::has('login'))
              @auth
              <li class="list-group-item">
                <a class="" href="#">HOME</a>
              </li>
              @else
              <li class="list-group-item">
                <a class="" href="#">LOGIN</a>
              </li>
              @if (Route::has('register'))
              <li class="list-group-item">
                <a class="" href="#">REGISTER</a>
              </li>
              @endif
                @endauth
              @endif
            </ul>
            </div>
            <div class="col-4">
            <i class="fa fa-facebook-official" style="font-size:24px"></i>
            <i class="fa  fa-instagram" style="font-size:24px"></i>
            <i class="fa fa-twitter-square" style="font-size:24px"></i>
            </div>
          </div>
        </div>
        <div class="row text-center">
            Copyright 2020 - Tous droits réservés. 
        </div>

      </section>
      <footer class="footer-section">
        <div class="container">
            <div class="footer-top">
                <div class="logo">
                    <a href="index-1.html">
                        <img src="http://pixner.net/boleto/demo/assets/images/footer/footer-logo.png" alt="footer">
                    </a>
                </div>
                <ul class="social-icons">
                    <li>
                        <a href="#0">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#0" class="">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#0">
                            <i class="fab fa-pinterest-p"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#0" class="active">
                            <i class="fab fa-google"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#0" class="">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="footer-bottom">
                <div class="footer-bottom-area">
                    <div class="left">
                        <p>Copyright © 2020.All Rights Reserved By <a href="#0">Boleto </a></p>
                    </div>
                    <ul class="links">
                        <li>
                            <a href="#0">About</a>
                        </li>
                        <li>
                            <a href="#0">Terms Of Use</a>
                        </li>
                        <li>
                            <a href="#0">Privacy Policy</a>
                        </li>
                        <li>
                            <a href="#0">FAQ</a>
                        </li>
                        <li>
                            <a href="#0">Feedback</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
      {{-- ::::::::::::::::::::::::FOOTER END HERE:::::::::::::::::::::--}}
      <script src="https://www.klevermedia.co.uk/html_templates/movie_star_html/js/jquery-2.2.4.min.js"></script>
      <script src="https://www.klevermedia.co.uk/html_templates/movie_star_html/js/jquery-ui.min.js"></script>
      <script src="https://www.klevermedia.co.uk/html_templates/movie_star_html/js/bootstrap.min.js"></script>
      <script src="https://www.klevermedia.co.uk/html_templates/movie_star_html/js/headhesive.min.js"></script>
      <script src="https://www.klevermedia.co.uk/html_templates/movie_star_html/js/matchHeight.min.js"></script>
      <script src="https://www.klevermedia.co.uk/html_templates/movie_star_html/js/modernizr.custom.js"></script>
      <script src="https://www.klevermedia.co.uk/html_templates/movie_star_html/js/slick.min.js"></script>
      <script src="https://www.klevermedia.co.uk/html_templates/movie_star_html/js/venobox.min.js"></script>
      <script src="https://use.fontawesome.com/4dfd2d448a.js"></script>
      <script src="https://www.klevermedia.co.uk/html_templates/movie_star_html/js/custom.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
      <script></script>
   </body>
</html>