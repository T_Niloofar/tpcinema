@extends('layouts.app')

@section('content')
<div class="container mt-5">
  <div class="row">
      <div class="col-lg-12">
          <div class="article-section padding-bottom">
              <div class="section-header-1">
                  <h2 class="title">movies</h2>
              </div>
              <div class="row mb-30-none justify-content-center">
              @foreach ($filmes as $filme)
                  <div class="col-sm-6 col-lg-3">
                      <div class="movie-grid">
                          <div class="movie-thumb c-thumb">
                              <a href="{{URL::to('filmes/' . $filme->id)}}">
                                  <img src="{{$filme->img}}" alt="movie">
                              </a>
                          </div>
                          <div class="movie-content bg-one">
                              <h5 class="title m-0">
                                  <a href="{{URL::to('filmes/' . $filme->id)}}">{{$filme->name}}</a>
                              </h5>
                              <ul class="movie-rating-percent">
                                  <li>
                                      <div class="thumb">
                                      Durée:<span class="content">{{$filme->duration}} min</span>
                                      </div>
                                      
                                  </li>
                                  <li>
                                      <div class="thumb">
                                      <span class="content">{{$filme->year}}</span>
                                      </div>
                                  </li>
                              </ul>
                          </div>
                      </div>
                  </div>
                @endforeach
              </div>
          </div>
      </div>
  </div>
</div>{{--- container main--}}
@endsection