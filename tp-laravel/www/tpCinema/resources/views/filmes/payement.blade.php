
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/all.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{ asset('css/flaticon.css')}}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{ asset('css/odometer.css')}}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/nice-select.css')}}">
    <link rel="stylesheet" href="{{ asset('css/main.css')}}">

    <link rel="shortcut icon" href="http://pixner.net/boleto/demo/assets/images/favicon.png" type="image/x-icon">

    <title>Boleto  - Online Ticket Booking Website HTML Template</title>
 

</head>

<body> 
    <!-- ==========Overlay========== -->
    <div class="overlay"></div>
    <a href="#0" class="scrollToTop">
        <i class="fas fa-angle-up"></i>
    </a>
    <!-- ==========Overlay========== -->

    <!-- ==========Header-Section========== -->
    <nav class="navbar navbar-expand-md navbar-dark shadow-sm fixed-top navbarMenu">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                Cinema Ticket
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>
  
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto ml-auto ">
                    <li class="nav-item ">
                        <a class="nav-link mr-2"style="font-size:17px;" href="{{URL::to('filmes')}}">FILMS </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mr-2" style="font-size:17px;" href="#">CINÉMAS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mr-2" style="font-size:17px;" href="#">GENRE</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="font-size:17px;" href="#">ACCOUNT</a>
                    </li>
                </ul>
  
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
  
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
  
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
    <!-- ==========Header-Section========== -->

    <!-- ==========Banner-Section========== -->
    <section class="details-banner hero-area bg_img seat-plan-banner" data-background="http://pixner.net/boleto/demo/assets/images/banner/banner04.jpg">
        <div class="container">
            <div class="details-banner-wrapper">
                <div class="details-banner-content style-two">
            
                    <h3 class="title">{{$filme->name}}</h3>
                    <div class="tags">
                        <a href="#0">City Walk</a>
                        <a href="#0">English - 2D</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ==========Banner-Section========== -->

    <!-- ==========Page-Title========== -->
    <section class="page-title bg-one">
        <div class="container">
            <div class="page-title-area">
                <div class="item md-order-1">
                    <a href="movie-ticket-plan.html" class="custom-button back-button">
                        <i class="flaticon-double-right-arrows-angles"></i>back
                    </a>
                </div>
                <div class="item date-item">
                    <span class="date">{{$ticket->date}}</span>
                <span class="custom-button">{{$ticket->seance}}</span>
                </div>
                <div class="item">
                    Prix <span id="prix">{{$ticket->prix}}</span> CHF
                </div>
            </div>
        </div>
    </section>
    <!-- ==========Page-Title========== -->

    <!-- ==========Movie-Section========== -->
    <div class="seat-plan-section padding-bottom padding-top">
        <div class="container">
            <div class="screen-area">
                <h4 class="screen">screen</h4>
                <div class="screen-thumb">
                    <img src="http://pixner.net/boleto/demo/assets/images/movie/screen-thumb.png" alt="movie">
                </div>
                
                <div class="screen-wrapper">
                    <ul class="seat-area couple">
                        @for ($i = 0; $i < $ticket->nombre/8; $i++)
                            <li class="seat-line">  
                                <ul class="seat--area">
                                    @for ($j = 0; $j < 8; $j++)
                                    <li class="front-seat">
                                        <ul>
                                            @php
                                            if ($i==0)  { $seat = $j+1; }
                                            else        { $seat = $i*8+1+$j; }
                                            @endphp
                                            <li class="single-seat ">
                                                <img class="seat" seatNumber="{{$seat}}" src="http://pixner.net/boleto/demo/assets/images/movie/seat01.png" alt="seat">
                                                <span class="sit-num">{{$seat}}</span>
                                            </li>
                                        </ul>
                                    </li>
                                    @endfor
                                </ul> 
                            </li>     
                        @endfor
                    </ul>
                </div>
            </div>

            <div class="proceed-book bg_img" data-background="http://pixner.net/boleto/demo/assets/images/movie/movie-bg-proceed.jpg">
                <div class="proceed-to-book">
                    <div class="book-item">
                        <span>You have Choosed Seat</span>
                        <h3 class="title" id="count"> </h3>
                    </div>
                    <div class="book-item">
                        <span>total price</span>
                        <h3 class="title" id="total"> </h3>
                    </div>
                    <div class="book-item">
                        <form action="{{URL::to('cart/')}}" method="post">
                            @csrf
                        <input type="hidden" id="seatNumbers" name="seatNumbers" value="">
                        <input type="hidden" name="ticket_id" value="{{$ticket->id}}">
                        <input type="hidden" name="user_id" value="2">
                        <input type="hidden" id="quantity" name="quantity" value="">
                        <button type="submit" class="custom-button">proceed</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        const seats = document.querySelectorAll(".single-seat .seat:not(.occupied)");
        const count = document.getElementById("count");
        const total = document.getElementById("total");
        const screen = document.querySelector(".screen-wrapper");

        let ticketPrice = document.getElementById('prix').textContent
        console.log('htmltext', ticketPrice)
        //update total and count
        function updateSelectedCount() {
        const selectedSeats = document.querySelectorAll(".seat.selected");
        const selectedSeatsCount = selectedSeats.length; 
        // Count number of the seat then give the value of 'quantity' input
        document.getElementById('quantity').value = selectedSeats.length;

        // meaning copy the value in selectedSeats using the spread operator
        const seatsIndex = [...selectedSeats].map(function(seat) {
            return [...seats].indexOf(seat);

        });
        // get number of the seat then give the value of 'seatNumbers' input
        let seatNumbers = document.getElementById('seatNumbers')
        seatNumbers.value = JSON.stringify(seatsIndex)
        localStorage.setItem("selectedSeats", JSON.stringify(seatsIndex));
          
        //copy selected seats into arr

        //Map through array

        //return a new array indexes
        count.innerText = selectedSeatsCount;
        total.innerText = selectedSeatsCount * ticketPrice;
        }
 
        //Seat select event
        screen.addEventListener("click", function(e) {
        if (
            e.target.classList.contains("seat") &&
            !e.target.classList.contains("occupied")
        ) {
            e.target.classList.toggle("selected");
            updateSelectedCount();
        }
        });

    </script>
    <!-- ==========Movie-Section========== -->

 
    <!-- ==========Newslater-Section========== -->


    <script src="http://pixner.net/boleto/demo/assets/js/jquery-3.3.1.min.js"></script>
    <script src="http://pixner.net/boleto/demo/assets/js/modernizr-3.6.0.min.js"></script>
    <script src="http://pixner.net/boleto/demo/assets/js/plugins.js"></script>
    <script src="http://pixner.net/boleto/demo/assets/js/bootstrap.min.js"></script>
    <script src="http://pixner.net/boleto/demo/assets/js/isotope.pkgd.min.js"></script>
    <script src="http://pixner.net/boleto/demo/assets/js/magnific-popup.min.js"></script>
    <script src="http://pixner.net/boleto/demo/assets/js/owl.carousel.min.js"></script>
    <script src="http://pixner.net/boleto/demo/assets/js/wow.min.js"></script>
    <script src="http://pixner.net/boleto/demo/assets/js/countdown.min.js"></script>
    <script src="http://pixner.net/boleto/demo/assets/js/odometer.min.js"></script>
    <script src="http://pixner.net/boleto/demo/assets/js/viewport.jquery.js"></script>
    <script src="http://pixner.net/boleto/demo/assets/js/nice-select.js"></script>
    <script src="http://pixner.net/boleto/demo/assets/js/main.js"></script>
</body>

</html>