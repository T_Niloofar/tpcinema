  @extends('layouts.app')
  @section('content')
      
    <!-- ==========Banner-Section========== -->
    <section class="details-banner hero-area bg_img seat-plan-banner" data-background="http://pixner.net/boleto/demo/assets/images/banner/banner04.jpg">
        <div class="container">
            <div class="details-banner-wrapper">
                <div class="details-banner-content style-two">
                    <h3 class="title">Film : {{$filme->name}}</h3>
                    <div class="tags">
                        <a href="#0">Cinéma: City Walk</a>
                        <a href="#0">English - 2D</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ==========Banner-Section========== -->

    <!-- ==========Page-Title========== -->
    <section class="page-title bg-one">
        <div class="container">
            <div class="page-title-area">
                <div class="item md-order-1">
                     
                </div>
                <div class="item "> <p>Date:</p> 
                    <h5 class="title">{{$ticket->date}}</h5>
                </div>
                <div class="item"><p>Seance: </p>
                    <h5 class="title">{{$ticket->seance}}</h5>
                </div>
                <div class="item"><p>Salon: </p>
                    <h5 class="title">{{$ticket->salon_id}}</h5>
                </div>
            </div>
        </div>
    </section>
    <!-- ==========Page-Title========== -->

    <!-- ==========Movie-Section========== -->
    <div class="movie-facility padding-bottom padding-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="checkout-widget d-flex flex-wrap align-items-center justify-cotent-between">
                        <div class="title-area">
                            <h5 class="title">Déjà un compte?</h5>
                        </div>
                        <a href="#0" class="sign-in-area">
                            <i class="fas fa-user"></i><span>Sign in</span>
                        </a>
                    </div>
                    <div class="checkout-widget checkout-contact">
                        <h5 class="title">Vos coordonnées </h5>
                        <form class="checkout-contact-form">
                            <div class="form-group">
                                <input type="text" placeholder="Nom prènom">
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Adresse e-mail">
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Numero de telephone">
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Continue" class="custom-button">
                            </div>
                        </form>
                    </div>
                    <div class="checkout-widget checkout-card mb-0"> 
                        
                        <h6 class="subtitle">Enter Your Card Details </h6>
                        <form class="payment-card-form">
                            <div class="form-group w-100">
                                <label for="card1">Card Details</label>
                                <input type="text" id="card1">
                                <div class="right-icon">
                                    <i class="flaticon-lock"></i>
                                </div>
                            </div>
                            <div class="form-group w-100">
                                <label for="card2"> Name on the Card</label>
                                <input type="text" id="card2">
                            </div>
                            <div class="form-group">
                                <label for="card3">Expiration</label>
                                <input type="text" id="card3" placeholder="MM/YY">
                            </div>
                            <div class="form-group">
                                <label for="card4">CVV</label>
                                <input type="text" id="card4" placeholder="CVV">
                            </div>
                        </form>
                        
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="booking-summery bg-one">
                        <h4 class="title">RÉSUMÉ DE VOTRE COMMANDE</h4>
                        <ul>
                            <li>
                                <h6 class="subtitle">{{$filme->name}}</h6>
                                <span class="info">English-2d</span>
                            </li>
                            <li>
                                <h6 class="subtitle"><span>{{$ticket->cinema_id}}</span><span>{{$ticket->cinema_id}}</span></h6>
                            <div class="info"><span>{{$ticket->date}} {{$ticket->seance}}</span> <span>Tickets</span></div>
                            </li>
                            <li>
                                <h6 class="subtitle mb-0"><span>Tickets  Price</span><span>{{$ticket->prix}}</span></h6>
                            </li>
                        </ul>
                       
                        <ul class="side-shape">
                            <li>
                                <span class="info"><span>price</span><span>{{$ticket->prix*$ordre['quantity']}}</span></span>
                                <span class="info"><span>Frais d'achat cinéma</span><span>00 .-</span></span>
                            </li>
                        </ul>
                    </div>
                    <div class="proceed-area  text-center">
                        <h6 class="subtitle"><span>Amount Payable</span><span>{{$ticket->prix*$ordre['quantity']}}</span></h6>
                        <form action="{{URL::to('cart/confirm')}}" method="post">
                            @csrf
                        <input type="hidden" name="seatNumbers" value="{{$ordre['seatNumbers']}}">
                        <input type="hidden" name="ticket_id" value="{{$ordre['ticket_id']}}">
                        <input type="hidden" name="user_id" value="{{$ordre['user_id']}}">
                        <input type="hidden" name="quantity" value="{{$ordre['quantity']}}">
                        <button type="submit" class="custom-button">Acheter</button>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ==========Movie-Section========== -->

    @endsection
