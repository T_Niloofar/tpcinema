@extends('layouts.app')

@section('content')
<div class="container mt-5">
  <div class="row">
      <div class="col-lg-12">
          <div class="article-section padding-bottom">
              <div class="section-header-1">
                  <h2 class="title">les cinémas</h2>
              </div>
              <div class="row mb-30-none justify-content-center">
              @foreach ($cinemas as $cinema)
                  <div class="col-sm-6 col-lg-3">
                      <div class="movie-grid">
                          <div class="movie-thumb c-thumb">
                              <a href="{{URL::to('filmes/' . $cinema->id)}}">
                                  <img src="{{$cinema->img}}" alt="movie">
                              </a>
                          </div>
                          <div class="movie-content bg-one">
                              <h5 class="title m-0">
                                  <p >{{$cinema->name}}</p>
                              </h5>
                          </div>
                      </div>
                  </div>
                @endforeach
              </div>
          </div>
      </div>
  </div>
</div>{{--- container main--}}
@endsection