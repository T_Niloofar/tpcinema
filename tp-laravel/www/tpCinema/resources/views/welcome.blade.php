<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Suisse Cinema</title>
      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
      {{--  
      <link rel="stylesheet" href="{{ asset('css/darkMovie.css') }}">
      --}}
      
      <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css')}}">
      <link rel="stylesheet" href="{{ asset('css/main.css')}}">
      <link rel="stylesheet" href="{{ asset('css/welcome.css') }}">

      <!-- script -->
      <script src="{{ asset('js/modal.js') }}" defer></script>
      <script src="{{ asset('js/script.js') }}" defer></script>
      <script src="{{ asset('js/carousel.js') }}" defer></script> 
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
     
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   </head>
   <body style="opacity: 1">
    <nav class="navbar navbar-expand-md navbar-dark shadow-sm fixed-top navbarMenu">
      <div class="container">
          <a class="navbar-brand" href="{{ url('/') }}">
              Cinema Ticket
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
              <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <!-- Left Side Of Navbar -->
              <ul class="navbar-nav mr-auto ml-auto ">
                  <li class="nav-item ">
                      <a class="nav-link mr-2"style="font-size:17px;" href="{{URL::to('filmes')}}">FILMS </a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link mr-2" style="font-size:17px;" href="{{URL::to('cinema')}}">CINÉMAS</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link mr-2" style="font-size:17px;" href="#">GENRE</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" style="font-size:17px;" href="#">ACCOUNT</a>
                  </li>
              </ul>

              <!-- Right Side Of Navbar -->
              <ul class="navbar-nav ml-auto">
                  <!-- Authentication Links -->
                  @guest
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                      </li>
                      @if (Route::has('register'))
                          <li class="nav-item">
                              <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                          </li>
                      @endif
                  @else
                      <li class="nav-item dropdown">
                          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                              {{ Auth::user()->name }} <span class="caret"></span>
                          </a>

                          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="{{ route('logout') }}"
                                 onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                  {{ __('Logout') }}
                              </a>

                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  @csrf
                              </form>
                          </div>
                      </li>
                  @endguest
              </ul>
          </div>
      </div>
  </nav>
     
      
      {{--      ..........................CAROSEL ......................... --}}
      <div id="carouselExampleCaptions" class="carousel slide mt-0" data-ride="carousel" style="border-top-right-radius: 1rem">
         <ol class="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="4"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="5"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="6"></li>
         </ol>
         <div class="carousel-inner">
            <div class="carousel-item active">
               <img src="{{URL::to('img/cin8.jpeg')}}" class="d-block w-100" alt="...">
               <div class="carousel-caption d-none d-md-block">
                  <h5>First slide label</h5>
                  <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
               </div>
            </div>
            <div class="carousel-item">
               <img src="{{URL::to('img/cin5.jpg')}}" class="d-block w-100" alt="...">
               <div class="carousel-caption d-none d-md-block">
                  <h5>First slide label</h5>
                  <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
               </div>
            </div>
            <div class="carousel-item">
               <img src="{{URL::to('img/2661193.jpg')}}" class="d-block w-100" alt="...">
               <div class="carousel-caption d-none d-md-block">
                  <h5>Second slide label</h5>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
               </div>
            </div>
            <div class="carousel-item">
               <img src="{{URL::to('img/cin7.jpg')}}" class="d-block w-100" alt="...">
               <div class="carousel-caption d-none d-md-block">
                  <h5>First slide label</h5>
                  <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
               </div>
            </div>
            <div class="carousel-item">
               <img src="{{URL::to('img/kore.jpg')}}" class="d-block w-100" alt="...">
               <div class="carousel-caption d-none d-md-block">
                  <h5>First slide label</h5>
                  <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
               </div>
            </div>
            <div class="carousel-item">
               <img src="{{URL::to('img/Spidey-3.jpg')}}" class="d-block w-100" alt="...">
               <div class="carousel-caption d-none d-md-block">
                  <h5>First slide label</h5>
                  <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
               </div>
            </div>
            <div class="carousel-item">
               <img src="{{URL::to('img/miles-morales-1.jpg')}}" class="d-block w-100" alt="...">
               <div class="carousel-caption d-none d-md-block">
                  <h5>Third slide label</h5>
                  <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
               </div>
            </div>
         </div>
         <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
         <span class="carousel-control-prev-icon" aria-hidden="true"></span>
         <span class="sr-only">Previous</span>
         </a>
         <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
         <span class="carousel-control-next-icon" aria-hidden="true"></span>
         <span class="sr-only">Next</span>
         </a>
      </div>

      {{-- ................... ACHAT IMMEDIAT........................      --}}
      <div class="contailer" style="width: 100%; margin:auto;border-radius:2em">
         <div class="row" style="margin:0 ;text-align:center;">
            <div class="m-quick-buy">
               <form action="{{ route('filmespayment') }}" method="POST">
                  @csrf
                  <ul class=" m-auto">
                     <li class="m-quick-buy__item" >
                        <select name="filmes_id" class="form-control dynamic" id="filmes_id"  data-dependent="cinemas">
                           <option >FILME</option>
                           @foreach ($filmes as $filme)
                           <option value="{{$filme->id}}">{{$filme->name}}</option>
                           @endforeach
                        </select>
                     </li>
                     <li class="m-quick-buy__item">
                        <select name="cinemas" class="form-control dynamic" id="cinemas"  data-dependent="date">
                           <option style="background: #5cb85c; color: #fff;" >CINEMA</option>
                        </select>
                     </li>
                     <li class="m-quick-buy__item">
                        <select name="date" class="form-control dynamic" id="date"  data-dependent="seance">
                           <option >DATE</option>
                        </select>
                     </li>
                     <li class="m-quick-buy__item" >
                        <select name="seance" class="form-control dynamic " id="seance">
                           <option >HORAIRE & VER...</option>
                        </select>
                     </li>
                     <li class="m-quick-buy__item" >
                        <button class="form-control btnAchat"  >ACHAT IMMEDIAT</button>
                     </li>
                  </ul>
               </form>
            </div>
         </div>
      </div>
            {{-- ::::::::::::::::::::SCRIPT ON MENU ACHAT:::::::::::::::::--}}
            <script>
                $(document).ready(function(){
                $('.dynamic').change(function(){
                if($(this).val() != '')
                 {
                     console.log('test');
                     var select = $(this).attr("id");
                     var value = $(this).val();
                     var dependent = $(this).data('dependent');
                     var _token = $('input[name="_token"]').val();
                     $.ajax({
                       url:"{{ route('filmSelect.fetch') }}",
                       method:"POST",
                       data:{select:select, value:value, _token:_token, dependent:dependent},
                       success:function(result)
                       {
                       $('#'+dependent).html(result);
                       }
                     })
                   }
                 });
                $('#filmes').change(function(){
                 $('#cinemas').val('');
                 $('#date').val('');
                });
                $('#cinemas').change(function(){
                 $('#date').val('');
                });
                $('#seance').change(function(){
                console.log('test');
                });
                });
             </script>
      {{-- ...........................................Images of films................................... --}}
      <div class="container mt-5">
        <div class="row">
          <div class="col-lg-12">
            <div class="article-section padding-bottom">
                <div class="section-header-1">
                    <h2 class="title">movies</h2>
                    <a class="view-all" href="{{URL::to('filmes')}}">View All</a>
                </div>
                <div class="row mb-30-none justify-content-center">
                  @foreach ($filmes as $filme)
                    <div class="col-sm-6 col-lg-3">
                        <div class="movie-grid">
                            <div class="movie-thumb c-thumb">
                                <a href="{{URL::to('filmes/' . $filme->id)}}">
                                    <img src="{{$filme->img}}" alt="movie">
                                </a>
                            </div>
                            <div class="movie-content bg-one">
                                <h5 class="title m-0">
                                    <a href="{{URL::to('filmes/' . $filme->id)}}">{{$filme->name}}</a>
                                </h5>
                                <ul class="movie-rating-percent">
                                    <li>
                                        <div class="thumb">
                                          Durée:<span class="content">{{$filme->duration}} min</span>
                                        </div>
                                        
                                    </li>
                                    <li>
                                        <div class="thumb">
                                         <span class="content">{{$filme->year}}</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
               @endforeach
                </div>
            </div>
        </div>

        </div>
      </div>{{--- container main--}}
      <div class="container">
        <div class="item">
          <div class="header">
              <div class="navigation">
                  <div class="cast-prev"><i class="flaticon-double-right-arrows-angles"></i></div>
                  <div class="cast-next"><i class="flaticon-double-right-arrows-angles"></i></div>
              </div>
          </div>
          <div class="casting-slider owl-carousel">

              <div class="cast-item">
                  <div class="cast-thumb">
                      <a href="#0">
                          <img src="http://pixner.net/boleto/demo/assets/images/cast/cast01.jpg" alt="cast">
                      </a>
                  </div>
                  <div class="cast-content">
                      <h6 class="cast-title"><a href="#0">Bill Hader</a></h6>
                      <span class="cate">actor</span>
                      <p>As Richie Tozier</p>
                  </div>
              </div>
              <div class="cast-item">
                  <div class="cast-thumb">
                      <a href="#0">
                          <img src="http://pixner.net/boleto/demo/assets/images/cast/cast02.jpg" alt="cast">
                      </a>
                  </div>
                  <div class="cast-content">
                      <h6 class="cast-title"><a href="#0">nora hardy</a></h6>
                      <span class="cate">actor</span>
                      <p>As raven</p>
                  </div>
              </div>
              <div class="cast-item">
                  <div class="cast-thumb">
                      <a href="#0">
                          <img src="http://pixner.net/boleto/demo/assets/images/cast/cast03.jpg" alt="cast">
                      </a>
                  </div>
                  <div class="cast-content">
                      <h6 class="cast-title"><a href="#0">alvin peters</a></h6>
                      <span class="cate">actor</span>
                      <p>As magneto</p>
                  </div>
              </div>
              <div class="cast-item">
                  <div class="cast-thumb">
                      <a href="#0">
                          <img src="http://pixner.net/boleto/demo/assets/images/cast/cast04.jpg" alt="cast">
                      </a>
                  </div>
                  <div class="cast-content">
                      <h6 class="cast-title"><a href="#0">josh potter</a></h6>
                      <span class="cate">actor</span>
                      <p>As quicksilver</p>
                  </div>
              </div>
            </div>
        </div>
      </div>
      <footer class="footer-section">
        <div class="container">
            <div class="footer-top">
                <div class="logo">
                    <a href="{{ url('/') }}">
                      CINEMA TICKETS
                    </a>
                </div>
                <ul class="social-icons">
                    <li>
                        <a href="#0">
                            <i class="fa fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#0" class="">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#0">
                            <i class="fa fa-pinterest-p"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#0">
                            <i class="fa fa-google"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#0" class="">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="footer-bottom">
                <div class="footer-bottom-area">
                    <div class="left">
                        <p>Copyright © 2020.All Rights Reserved By <a href="#0">Boleto </a></p>
                    </div>
                    <ul class="links">
                        <li>
                            <a href="#0">About</a>
                        </li>
                        <li>
                            <a href="#0">Terms Of Use</a>
                        </li>
                        <li>
                            <a href="#0">Privacy Policy</a>
                        </li>
                        <li>
                            <a href="#0">FAQ</a>
                        </li>
                        <li>
                            <a href="#0">Feedback</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
      {{-- ::::::::::::::::::::::::FOOTER END HERE:::::::::::::::::::::--}}
      
      <script src="http://pixner.net/boleto/demo/assets/js/owl.carousel.min.js"></script>
      <script src="https://use.fontawesome.com/4dfd2d448a.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
     
   </body>
</html>