<!--slider menu-->
<div class="sidebar-menu">
  <div class="logo"> <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="#"> <span id="logo" ></span> 
      <!--<img id="logo" src="" alt="Logo"/>--> 
  </a> </div>		  
  <div class="menu">
    <ul id="menu" >
      <li id="menu-home" ><a href="index.html"><i class="fa fa-tachometer"></i><span>Dashboard</span></a></li>
      <li><a href="#"><i class="fas fa-camera-movie"></i><span>Cinémas</span><span class="fa fa-angle-right" style="float: right"></span></a>
        <ul>
          <li><a href="grids.html">List</a></li>
              <li><a href="{{ url('/cinema/create') }}">Ajouter</a></li>
        </ul>
      </li>
      <li id="menu-comunicacao" ><a href="#"><i class="fa fa-film"></i><span>Films</span><span class="fa fa-angle-right" style="float: right"></span></a>
        <ul id="menu-comunicacao-sub" >
          <li id="menu-mensagens" style="width: 120px" ><a href="buttons.html">List</a>		              
          </li>
          <li id="menu-arquivos" ><a href="{{ url('/filmes/create') }}">Ajouter</a></li>
        </ul>
      </li>
      <li id="menu-academico" ><a href="#"><i class="fa fa-file-text"></i><span>Salons</span><span class="fa fa-angle-right" style="float: right"></span></a>
        <ul id="menu-academico-sub" >
           <li id="menu-academico-boletim" ><a href="login.html">List</a></li>
          <li id="menu-academico-avaliacoes" ><a href="{{ url('/salons/create') }}">Ajouter</a></li>		           
        </ul>
      </li>
          <li><a href="#"><i class="fa fa-cog"></i><span>Ticket</span><span class="fa fa-angle-right" style="float: right"></span></a>
              <ul id="menu-academico-sub" >
                <li id="menu-academico-avaliacoes" ><a href="404.html">List</a></li>
                <li id="menu-academico-boletim" ><a href="blank.html">Commend</a></li>
             </ul>
         </li>
      <li><a href="#"><i class="fa fa-envelope"></i><span>Mailbox</span><span class="fa fa-angle-right" style="float: right"></span></a>
         <ul id="menu-academico-sub" >
            <li id="menu-academico-avaliacoes" ><a href="inbox.html">Inbox</a></li>
            <li id="menu-academico-boletim" ><a href="inbox-details.html">Compose email</a></li>
           </ul>
      </li>
      
       <li><a href="#"><i class="fa fa-shopping-cart"></i><span>E-Commerce</span><span class="fa fa-angle-right" style="float: right"></span></a>
         <ul id="menu-academico-sub" >
            <li id="menu-academico-avaliacoes" ><a href="product.html">Product</a></li>
            <li id="menu-academico-boletim" ><a href="price.html">Price</a></li>
           </ul>
       </li>
    </ul>
  </div>
</div>
<div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<script>
var toggle = true;
      
$(".sidebar-icon").click(function() {                
if (toggle)
{
$(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
$("#menu span").css({"position":"absolute"});
}
else
{
$(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
setTimeout(function() {
$("#menu span").css({"position":"relative"});
}, 400);
}               
          toggle = !toggle;
      });
</script>