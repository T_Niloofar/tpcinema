<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
     <!-- CSRF Token -->
     <meta name="csrf-token" content="{{ csrf_token() }}">

     <title>{{ config('app.name', 'Suisse Cinema') }}</title>
      
      <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css')}}">
      <link rel="stylesheet" href="{{ asset('css/main.css')}}">
      <link rel="stylesheet" href="{{ asset('css/welcome.css') }}">
      <link rel="stylesheet" href="{{ asset('css/magnific-popup.css')}}">
      <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">

      <!-- script -->
          <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script> 
        <script src='https://kit.fontawesome.com/a076d05399.js'></script>
        <script src="{{ asset('js/modal.js') }}" defer></script>
        <script src="{{ asset('js/script.js') }}" defer></script>
        <script src="{{ asset('js/carousel.js') }}" defer></script> 
   </head>
   <body style="opacity: 1">
    <nav class="navbar navbar-expand-md navbar-dark shadow-sm fixed-top navbarMenu">
      <div class="container">
          <a class="navbar-brand" href="{{ url('/') }}">
              Cinema Ticket
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
              <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <!-- Left Side Of Navbar -->
              <ul class="navbar-nav mr-auto ml-auto ">
                  <li class="nav-item ">
                      <a class="nav-link mr-2"style="font-size:17px;" href="{{URL::to('filmes')}}">FILMS </a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link mr-2" style="font-size:17px;" href="{{URL::to('cinema')}}">CINÉMAS</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link mr-2" style="font-size:17px;" href="#">GENRE</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" style="font-size:17px;" href="#">ACCOUNT</a>
                  </li>
              </ul>

              <!-- Right Side Of Navbar -->
              <ul class="navbar-nav ml-auto">
                  <!-- Authentication Links -->
                  @guest
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                      </li>
                      @if (Route::has('register'))
                          <li class="nav-item">
                              <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                          </li>
                      @endif
                  @else
                      <li class="nav-item dropdown">
                          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                              {{ Auth::user()->name }} <span class="caret"></span>
                          </a>

                          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="{{ route('logout') }}"
                                 onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                  {{ __('Logout') }}
                              </a>

                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  @csrf
                              </form>
                          </div>
                      </li>
                  @endguest
              </ul>
          </div>
      </div>
  </nav>
  <div id="app">
         

    <main class="py-4">
        @yield('content')
    </main>
</div>
    
      <footer class="footer-section">
        <div class="container">
            <div class="footer-top">
                <div class="logo">
                    <a href="{{ url('/') }}">
                      CINEMA TICKETS
                    </a>
                </div>
                <ul class="social-icons">
                    <li>
                        <a href="#0">
                            <i class="fa fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#0" class="">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#0">
                            <i class="fa fa-pinterest-p"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#0">
                            <i class="fa fa-google"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#0" class="">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="footer-bottom">
                <div class="footer-bottom-area">
                    <div class="left">
                        <p>Copyright © 2020.All Rights Reserved By <a href="#0">Boleto </a></p>
                    </div>
                    <ul class="links">
                        <li>
                            <a href="#0">About</a>
                        </li>
                        <li>
                            <a href="#0">Terms Of Use</a>
                        </li>
                        <li>
                            <a href="#0">Privacy Policy</a>
                        </li>
                        <li>
                            <a href="#0">FAQ</a>
                        </li>
                        <li>
                            <a href="#0">Feedback</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
      {{-- ::::::::::::::::::::::::FOOTER END HERE:::::::::::::::::::::--}}
      
      <script src="https://use.fontawesome.com/4dfd2d448a.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
      <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
      <script src="{{asset('js/magnific-popup.min.js')}}"></script>
      <script src="{{asset('js/owl.carousel.min.js')}}"></script>
      <script src="{{asset('js/wow.min.js')}}"></script>
      <script src="{{asset('js/nice-select.js')}}"></script>
      <script src="{{asset('js/main.js')}}"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
      


      
    
    <script src="{{asset('js/modernizr-3.6.0.min.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/isotope.pkgd.min.js')}}"></script>
    
    <script src="{{asset('js/viewport.jquery.js')}}"></script>
   </body>
</html>

 