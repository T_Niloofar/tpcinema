@extends('layouts.admin')

@section('adminPanel')
<!--market updates updates-->
<div class="market-updates">
    <div class="col-md-3 market-update-gd">
        <div class="market-update-block clr-block-1">
            <div class="col-md-8 market-update-left">
                <h3>{{$cinemas->count()}}</h3>
                <h4>Numero des Cinémas</h4>
            </div>
            <div class="col-md-4 market-update-right">
                <i class="fa fa-file-text-o"> </i>
            </div>
          <div class="clearfix"> </div>
        </div>
    </div>

    <div class="col-md-3 market-update-gd">
        <div class="market-update-block clr-block-2">
         <div class="col-md-8 market-update-left">
            <h3>{{$filmes->count()}}</h3>
            <h4>Numbre de Films</h4>
           
          </div>
            <div class="col-md-4 market-update-right">
                <i class="fa fa-eye"> </i>
            </div>
          <div class="clearfix"> </div>
        </div>
    </div>

    <div class="col-md-3 market-update-gd">
        <div class="market-update-block clr-block-3">
            <div class="col-md-8 market-update-left">
                <h3>{{$tickets->count()}}</h3>
                <h4>Numero de billet</h4>
            </div>
            <div class="col-md-4 market-update-right">
                <i class="fa fa-envelope-o"> </i>
            </div>
          <div class="clearfix"> </div>
        </div>
    </div>
    <div class="col-md-3 market-update-gd">
      <div class="market-update-block clr-block-2">
        <div class="col-md-8 market-update-left">
            <h3>{{$filmes->count()}}</h3>
            <h4>Numbre de Films</h4>
          </div>
          <div class="col-md-4 market-update-right">
                <i class="fa fa-film"> </i>
          </div>
          <div class="clearfix"> </div>
          </div>
    </div>
   <div class="clearfix"> </div>
</div>
<!--market updates end here-->

<!--mainpage chit-chating-->
<div class="chit-chat-layer1">
	<div class="col-md-6 chit-chat-layer1-left">
               <div class="work-progres">
                            <div class="chit-chat-heading">
                                  Le list de cinémas
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th>Name</th>
                                      <th>ville</th>
                                      <th>Modifier</th>
                                      <th>Suprimer</th>
                                  </tr>
                              </thead>
                              <tbody>
                                @foreach ($cinemas as $cinema)
                                  <tr>
                                    <td>{{$cinema->name}}</td>
                                    <td>{{$cinema->city}}</td>
                                    <td><button>modifier</button></td>                                 
                                    <td><button>Suprimer</button></td>
                                </tr>
                              @endforeach
                          </tbody>
                      </table>
                  </div>
             </div>
      </div>
     
     <div class="clearfix"> </div>
</div>
<!--main page chit chating end here-->
    
@endsection