@extends('layouts.admin')

@section('adminPanel')
  <div class="container">
      <div class="row">
          <h1>ajoutter un Salon</h1>
      </div>
      <div class="row">
          <form action="http://localhost/salons" method="POST">
            @csrf
              <div class="form-row">
                <div class="col-md-6 mb-3">
                  <label for="validationServer01">Nom</label>
                  <input name="name" type="text" class="form-control is-valid" id="validationServer01"  required>
                </div>
                <div class="col-md-6 mb-3">
                  <label for="validationServer02">Cinema</label>
                  <select name="cinema_id" class="form-control dynamic" id=""  >
                    <option >Cinema</option>
                    @foreach ($cinemas as $cinema)
                    <option value="{{$cinema->id}}">{{$cinema->name}}</option>
                    @endforeach
                 </select> 
                </div>
              </div>
              <div class="form-row">
                <div class="col-md-6 mb-3">
                  <label for="validationServer03">Capasité</label>
                  <input name="capasite" type="number" class="form-control is-invalid" id="validationServer03"required>
                </div>
              </div>
              <button class="btn btn-primary" type="submit">Submit form</button>
            </form>
      </div>
  </div>
 @endsection