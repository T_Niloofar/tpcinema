@extends('layouts.admin')

@section('adminPanel')

<div class="container">
  <div class="row">
      <h1>Ajouter un film</h1>
  </div>
  <div class="row">
      <form action="http://localhost/filmes" method="POST">
        @csrf
          <div class="form-row">
            <div class="col-md-6 mb-3">
              <label for="validationServer01">Nom</label>
              <input name="name" type="text" class="form-control is-valid" id="validationServer01"  required>
            </div>
            <div class="col-md-6 mb-3">
              <label for="validationServer02">Description</label>
              <input name="description" type="text" class="form-control is-valid" id="validationServer02"  required>
            </div>
          </div>
          <div class="form-row">
            <div class="col-md-6 mb-3">
              <label for="validationServer03">Directeur</label>
              <input name="directeur" type="text" class="form-control is-invalid" id="validationServer03"required>
            </div>
            <div class="col-md-6 mb-6">
              <label for="validationServer04">Duration</label>
              <input name="duration" type="number" class="form-control is-invalid" id="validationServer03"required>
            </div>
          </div>
          <div class="form-row">
              <div class="col-md-6 mb-3">
                <label for="validationServer01">Genre</label>
                <input name="genre" type="text" class="form-control is-valid" id="validationServer01"  required>
              </div>
              <div class="col-md-6 mb-3">
                <label for="validationServer02">Salon</label>
                <input name="salon_id" type="number" class="form-control is-valid" id="validationServer02"  required>
              </div>
            </div>
            <div class="form-row">
              <div class="col-md-4 mb-3">
                <label for="validationServer03">Tiser</label>
                <input name="tiser" type="text" class="form-control is-invalid" id="validationServer03"required>
              </div>
              <div class="col-md-4 mb-6">
                <label for="validationServer04">Image</label>
                <input name="img" type="text" class="form-control is-invalid" id="validationServer03"required>
              </div>
              <div class="col-md-4 mb-6">
                  <label for="validationServer04">Year</label>
                  <input name="year" type="number" class="form-control is-invalid" id="validationServer03"required>
                </div>
            </div>
          
          <button class="btn btn-primary" type="submit">Submit form</button>
        </form>
  </div>
</div>

@endsection
