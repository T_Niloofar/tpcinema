@extends('layouts.admin')

@section('adminPanel')
  <div class="container">
      <div class="row">
          <h1>ajoutter un cinéma</h1>
      </div>
      <div class="row">
          <form action="http://localhost/cinema" method="POST">
            @csrf
              <div class="form-row">
                <div class="col-md-6 mb-3">
                  <label for="validationServer01">Nom</label>
                  <input name="name" type="text" class="form-control is-valid" id="validationServer01"  required>
                </div>
                <div class="col-md-6 mb-3">
                  <label for="validationServer02">Adresse</label>
                  <input name="adresse" type="text" class="form-control is-valid" id="validationServer02"  required>
                </div>
              </div>
              <div class="form-row">
                <div class="col-md-6 mb-3">
                  <label for="validationServer03">Ville</label>
                  <input name="city" type="text" class="form-control is-invalid" id="validationServer03"required>
                </div>
                <div class="col-md-6 mb-6">
                  <label for="validationServer04">Image</label>
                  <input name="img" type="text" class="form-control is-invalid" id="validationServer03"required>
                </div>
              </div>
              
              <button class="btn btn-primary" type="submit">Submit form</button>
            </form>
      </div>
  </div>
 @endsection